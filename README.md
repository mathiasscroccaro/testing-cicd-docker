# CICD template for gitlab pipelines

## How to install

1. Download the installer and execute: 

`sudo dpkg -i $installer_path`

2. Create a user with home directory: 

`sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash`

3. Install the gitlab-runner user:

`sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner`

4. Register the runner:

```
sudo gitlab-runner register -n \
--url https://gitlab.com/ \
--registration-token $TOKEN_FROM_GITLAB_WEBSITE \
--executor docker \
--description "My Docker Runner" \
--docker-image "docker:stable" \
--docker-privileged \
--docker-volumes "/certs/client"

```
5. Verify if the service is running:

`sudo service gitlab-runner status`

6. Now you can use the docker-compose commands on your pipeline!
